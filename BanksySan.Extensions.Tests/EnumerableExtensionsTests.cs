﻿namespace BanksySan.Extensions.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;

    [TestFixture]
    public class EnumerableExtensionsTests
    {
        private static readonly IEnumerable<int> DUMMY_FIRST = new[] { 1, 2, 3, 4, 5 };
        private static readonly IEnumerable<string> DUMMY_SECOND_MATCHS_ALL = new[] { "1", "2", "3", "4", "5" };
        private static readonly IEnumerable<string> DUMMY_SECOND_MATCHS_NONE = Enumerable.Empty<string>();

        [Test]
        public void MatchAll()
        {
            var actualExcepts = DUMMY_FIRST.Except(DUMMY_SECOND_MATCHS_ALL,
                (first, second) =>
                {
                    var secondAsInt = int.Parse(second);

                    return first == secondAsInt;
                });

            Assert.That(actualExcepts, Is.Empty);
        }

        [Test]
        public void MatchNone()
        {
            var actualExcepts = DUMMY_FIRST.Except(DUMMY_SECOND_MATCHS_NONE,
                (first, second) =>
                {
                    var secondAsInt = int.Parse(second);

                    return first == secondAsInt;
                });

            Assert.That(actualExcepts, Is.EqualTo(DUMMY_FIRST));
        }

        [Test]
        public void EmptyFirst()
        {
            var actualExcepts = Enumerable.Empty<int>().Except(DUMMY_SECOND_MATCHS_ALL,
                (first, second) =>
                {
                    var secondAsInt = int.Parse(second);

                    return first == secondAsInt;
                });

            Assert.That(actualExcepts, Is.EqualTo(Enumerable.Empty<int>()));
        }

        [Test]
        public void EmptySecond()
        {
            var actualExcepts = DUMMY_FIRST.Except(Enumerable.Empty<string>(),
                (first, second) =>
                {
                    var secondAsInt = int.Parse(second);

                    return first == secondAsInt;
                });

            Assert.That(actualExcepts, Is.EqualTo(DUMMY_FIRST));
        }

        [Test]
        public void NullsThrowException()
        {
            var e = Assert.Throws<ArgumentNullException>(() => DUMMY_FIRST.Except((IEnumerable<object>) null, (i, j) => true));
            
            Assert.That(e.Message, Is.EqualTo("Value cannot be null.\r\nParameter name: second"));
        }
    }
}