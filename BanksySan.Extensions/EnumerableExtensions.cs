﻿namespace BanksySan.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class EnumerableExtensions
    {
        public static IEnumerable<TFirst> Except<TFirst, TSecond>(this IEnumerable<TFirst> first,
                                                                  IEnumerable<TSecond> second,
                                                                  Func<TFirst, TSecond, bool> comparitor)
        {
            if (first == null)
            {
                throw new ArgumentNullException(nameof(first));
            }

            if (second == null)
            {
                throw new ArgumentNullException(nameof(second));
            }

            if (comparitor == null)
            {
                throw new ArgumentNullException(nameof(comparitor));
            }

            var exclusiveItems = new List<TFirst>();

            foreach (var firstItem in first)
            {
                if (second.Any(secondItem => comparitor(firstItem, secondItem)))
                {
                    continue;
                }

                exclusiveItems.Add(firstItem);
            }

            return exclusiveItems.ToArray();
        }
    }
}